package pl.edu.agh.qa.library;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import pl.edu.agh.qa.library.utils.DisplayedItem;
import pl.edu.agh.qa.library.utils.PrintHandler;
import pl.edu.agh.base.*;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ImportTest {
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }


    @Test
    public void itemsShouldBeImportedFromFile() {
        library.importItemsFromFile("books.csv");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;1\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n" +
                "National Geographic;01/2011;5;5\r\n");

        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void magazineShouldBeAddedToImportedFromFile() {
        library.importItemsFromFile("books.csv");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;1\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n" +
                "National Geographic;01/2011;6;6\r\n");

        library.addItemToLibrary(nationalGeographic);
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void newMagazineShouldBeAddedToImportedFromFile() {
        library.importItemsFromFile("books.csv");
        Item nationalGeographic = new Magazine("02/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;1\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n" +
                "National Geographic;01/2011;5;5\r\n" +
                "National Geographic;02/2011;1;1\r\n");

        library.addItemToLibrary(nationalGeographic);
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void bookShouldBeAddedIToImportedFromFile() {
        library.importItemsFromFile("books.csv");
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;2;2\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n" +
                "National Geographic;01/2011;5;5\r\n");

        library.addItemToLibrary(graOTron);
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void newBookShouldBeAddedIToImportedFromFile() {
        library.importItemsFromFile("books.csv");
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n"
                + "Gra o tron;George’a R.R. Martin;1;1\r\n"
                + "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n"
                + "National Geographic;01/2011;5;5\r\n"
                + "Mastering Selenium WebDriver;Mark Collin;1;1\r\n");

        library.addItemToLibrary(seleniumWebDriver);
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void c() {
        library.importItemsFromFile("books.csv");

        library.importItemsFromFile("books.csv");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList(
                "Ogniem i mieczem;H. Sienkiewicz;40;40\r\n" +
                        "Tożsamość Bourne’a;R. Ludlum;20;20\r\n" +

                        "Gra o tron;George’a R.R. Martin;2;2\r\n" +
                        "National Geographic;01/2011;10;10\r\n");

        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }
}
