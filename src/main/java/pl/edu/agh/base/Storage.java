package pl.edu.agh.base;

import java.util.ArrayList;

public class Storage {
    private ArrayList<User> UserLibrary;
    private ArrayList<Item> ItemLibrary;
    private ArrayList<Reservation> RentLibrary;

    public Storage() {
        UserLibrary = new ArrayList<>();
        ItemLibrary = new ArrayList<>();
        RentLibrary = new ArrayList<>();
    }

    public void insertUserToDB(User user) {
        UserLibrary.add(user);
    }

    public boolean checkIfItemExists(Item i) {

        for (Item item : ItemLibrary) {
//            if (item.getTitle().equals(i.getTitle()) && item.getAdditionalProperty().equals(i.getAdditionalProperty())) {
            if (checkIfItemsAreEquals(item, i)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfItemsAreEquals(Item firstitem, Item seconditem) {
        return firstitem.getTitle().equals(seconditem.getTitle()) &&
                firstitem.getAdditionalProperty().equals(seconditem.getAdditionalProperty());

    }

    public int getItems(Item i) {

        for (Item item : ItemLibrary) {
            if (checkIfItemsAreEquals(item, i)) {
                return item.getItems();
            }
        }

        return 0;
    }

    public void updateItem(Item it) {
        int i = 1;
        for (Item item : ItemLibrary) {
            final int allitems = item.getItems();
            final int itemsleft = item.getItemsleft();
            final int itemsToAdd = it.getItems();
            if (checkIfItemsAreEquals(item, it)) {
                item.setItems(allitems + it.items);
                item.setItemsleft(itemsleft + it.getItems());

            }
//            ItemLibrary.remove(item);
//            ItemLibrary.add(item);

        }

    }

    public void insertItemToDB(Item i) {
        ItemLibrary.add(i);
    }

    private boolean checkUserEquality(User firstuser, User seconduser) {
        if (!firstuser.getCardNumber().equals(seconduser.getCardNumber())) return false;
        if (!firstuser.getFirstName().equals(seconduser.getFirstName())) return false;
        if (!firstuser.getLastName().equals(seconduser.getLastName())) return false;
        return true;
    }

    public Boolean checkIfUserExists(User u) {
        for (User user : UserLibrary) {
            if (checkUserEquality(user, u)) return true;
        }
        return false;
    }

    public int getItemsleft(Item i) {

        for (Item item : ItemLibrary) {
            if (checkIfItemsAreEquals(item, i)) return item.getItemsleft();
        }
        return 0;
    }

    public void insertBooking(Item i, User u) {
        RentLibrary.add(new Reservation(u, i));
        for(User user : UserLibrary){
            if(checkUserEquality(user,u)){
                user.setHowManyItemsCanBeRented(user.getHowManyItemsCanBeRented() - 1);
            }
        }
        for (Item item : ItemLibrary){
            if(checkIfItemsAreEquals(item,i)){
                item.setItemsleft(item.getItemsleft()-1);
            }
        }

    }

    public int getItemsBooked(User u) {
        int itemsBooked = 0;
        for (Reservation r : RentLibrary) {
            if (r.getUserID().equals(u.getCardNumber())) {
                itemsBooked++;
            }
        }

        return itemsBooked;
    }

    public ArrayList<Magazine> getMagazineDetails() {
        ArrayList lista = new ArrayList();
        for (Item i : ItemLibrary) {
            if (i.name().equals("Magazine")) lista.add(i);
        }
        return lista;
    }

    public ArrayList<Book> getBookDetails() {
        ArrayList lista = new ArrayList();
        for (Item i : ItemLibrary) {
            if (i.name().equals("Book")) lista.add(i);

        }
        return lista;
    }


    public ArrayList<User> getUserDetails() {
        ArrayList lista = new ArrayList();
        for (User u : UserLibrary) {
            lista.add(u);
        }

        return lista;
    }

    public ArrayList<String> getUserCardNumbers() {
        ArrayList<String> lista = new ArrayList<>();
        for (User u : UserLibrary) {
            lista.add(u.getCardNumber());
        }
        return lista;
    }

    public ArrayList<Item> getUserBooking(String cardnumber) {
        ArrayList<Item> lista = new ArrayList();
        for (Reservation r : RentLibrary) {
            if (r.getUserID().equals(cardnumber)) lista.add(r.getItem());
        }
        return lista;
    }

}
