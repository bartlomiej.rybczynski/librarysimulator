package pl.edu.agh.base;

public class Lecturer extends User {
    @Override
    public int NumberOfPositionsToRent() {
        return 10;
    }

    public Lecturer(String FirstName, String LastName){
        super(FirstName, LastName);
    }
    @Override
    public String getName() {
        return "Lecturer";
    }

}
