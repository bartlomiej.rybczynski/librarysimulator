package pl.edu.agh.base;

public abstract class Item {
    String title;

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public int getItemsleft() {
        return itemsleft;
    }

    public void setItemsleft(int itemsleft) {
        this.itemsleft = itemsleft;
    }

    int itemsleft;

    int items;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public abstract String name();

    public abstract String getAdditionalProperty();

    public abstract void setAdditionalProperty(String property);


    public Item(String title) {
        setTitle(title);
        setItems(1);
        setItemsleft(1);
    }




}
