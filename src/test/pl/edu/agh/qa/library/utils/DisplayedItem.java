package pl.edu.agh.qa.library.utils;

import java.util.Objects;

public class DisplayedItem {
    private String title;
    private String authorOrNumber;
    private int allNumbers;
    private int availableItems;

    public DisplayedItem(String line) {
        String[] elements = line.split(";");
        title = elements[0].trim();
        authorOrNumber = elements[1].trim();
        allNumbers = Integer.parseInt(elements[2].trim());
        availableItems = Integer.parseInt(elements[3].trim());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisplayedItem that = (DisplayedItem) o;
        return allNumbers == that.allNumbers &&
                availableItems == that.availableItems &&
                Objects.equals(title, that.title) &&
                Objects.equals(authorOrNumber, that.authorOrNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, authorOrNumber, allNumbers, availableItems);
    }
}
