package pl.edu.agh.qa.library.utils;

import java.util.Objects;

public class DisplayedPerson {
    private String firstName;
    private String lastName;
    private String cardNr;
    private String type;

    public DisplayedPerson(String line) {
        String[] elements = line.split(";");
        firstName = elements[0].trim();
        lastName = elements[1].trim();
        cardNr = elements[2].trim();
        type = elements[3].trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCardNr() {
        return cardNr;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DisplayedPerson that = (DisplayedPerson) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {

        return Objects.hash(firstName, lastName, type);
    }
}
