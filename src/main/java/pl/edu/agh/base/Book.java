package pl.edu.agh.base;

public class Book extends Item {
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    String author;

    public Book(String Author, String title){
        super(title);
        setAuthor(Author);
    }

    @Override
    public String name() {
        return "Book";
    }

    @Override
    public String getAdditionalProperty() {
        return getAuthor();
    }

    @Override
    public void setAdditionalProperty(String author) {

    }
}
