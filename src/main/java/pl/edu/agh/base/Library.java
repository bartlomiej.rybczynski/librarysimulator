package pl.edu.agh.base;

import java.io.*;
import java.util.ArrayList;

public class Library {
    //    DB db = new DB();
    Storage db = new Storage();

    public Library() {


    }

    private void cleanDBfile() {
        File file = new File("library.db");
        file.delete();

    }


    public void addUserToLibrary(User... users) {
        for (User user : users) {
            db.insertUserToDB(user);
        }
    }

    public void addItemToLibrary(Item... items) {
Book b =null;
Magazine m = null;
        for (Item  arg : items) {
                if (db.checkIfItemExists( arg)) {
                    if (arg.name().equals("Book")){
                        b = new Book(arg.getAdditionalProperty(),arg.getTitle());
//                        b.setItems(arg.getItems());
                        db.updateItem(b);
                    } else {
                        m = new Magazine(arg.getAdditionalProperty(),arg.getTitle());
//                        m.setItems(arg.getItems());
                        db.updateItem(m);
                    }
                } else {
                    if (arg.getItems() == 0) {
                        arg.setItems(1);
                    } else {

                        arg.setItems(arg.getItems());
                    }

                    arg.setItemsleft(arg.getItems());//
                    db.insertItemToDB(arg);
                }
        }
    }


    public boolean rentItemToUser(Item item, User user) {
        if (!db.checkIfItemExists(item)) return false;
        if (!db.checkIfUserExists(user)) return false;
        if (db.getItemsleft(item) > 0 && db.getItemsBooked(user) < user.NumberOfPositionsToRent()) {
            db.insertBooking(item, user);
        } else {
            return false;
        }
        return true;
    }

    public void printListOfMagazines() {

        ArrayList<Magazine> magazine = new ArrayList();
        magazine = db.getMagazineDetails();
        for (int i = 0; i < magazine.size(); i++) {
            System.out.println(magazine.get(i).getTitle() + ";" + magazine.get(i).getNumber() + ";" + magazine.get(i).getItems() + ";" + magazine.get(i).getItemsleft());
        }
    }

    public void printListOfBooks() {
        ArrayList<Book> items = new ArrayList();
        items = db.getBookDetails();
        for (int i = 0; i < items.size(); i++) {
            System.out.println(items.get(i).getTitle() + ";" + items.get(i).getAuthor() + ";" + items.get(i).getItems() + ";" + items.get(i).getItemsleft());
        }
    }

    public void printListOfUsers() {
//        DB db = new DB();

        ArrayList<User> users = new ArrayList();
        users = db.getUserDetails();
        for (User user : users) {
            System.out.println(user.getFirstName() + ";" + user.getLastName() + ";" + user.getCardNumber() + ";" + user.getCardNumber().substring(0, 1));
        }


    }

    public void importItemsFromFile(String csvFile) {
        String linia = "";
        Book book = null;
        Magazine magazine = null;
        try {
            FileReader fr = new FileReader(csvFile);
            BufferedReader br = new BufferedReader(fr);

            while ((linia = br.readLine()) != null) {
                String[] newline = linia.split(";");
                if (newline[3].equals("B")) {
                    book = new Book(newline[1], newline[0]);
                    book.setItems(Integer.parseInt(newline[2]));
                    addItemToLibrary(book);
                    book=null;
                } else if (newline[3].equals("M")) {
                    magazine = new Magazine(newline[1], newline[0]);
                    magazine.setItems(Integer.parseInt(newline[2]));
                    addItemToLibrary(magazine);
                    magazine=null;
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exportUsersWithItemsToFile(String csvFile) {

        try {
            PrintWriter pw = new PrintWriter(csvFile);
            StringBuilder sb = new StringBuilder();
            ArrayList<String> cardnumnberlist = db.getUserCardNumbers();
            ArrayList<Item> items = null;
            for (String card : cardnumnberlist) {
                int i = 0;
                items = db.getUserBooking(card);
                if (items.size() > 0) {
                    sb.append(card + "[");
                }

                for (Item item : items) {
                    if (i != 0) {
                        sb.append("; ");
                    }
                    i++;
                    sb.append(item.getTitle() + "-" + item.getAdditionalProperty());
                }
                if (db.getUserBooking(card).size() > 0) {
                    sb.append("]\n");
                }
            }
            pw.write(sb.toString());
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

