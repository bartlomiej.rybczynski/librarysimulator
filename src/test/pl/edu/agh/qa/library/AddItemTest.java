package pl.edu.agh.qa.library;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import pl.edu.agh.qa.library.utils.DisplayedItem;
import pl.edu.agh.qa.library.utils.PrintHandler;
import pl.edu.agh.base.*;
import org.junit.contrib.java.lang.system.SystemOutRule;


import java.util.List;

import static org.junit.Assert.assertTrue;

public class AddItemTest {
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }

    @Test
    public void shouldBeAbleToAddBook() {
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Mastering Selenium WebDriver;Mark Collin;1;1\r\n");

        library.addItemToLibrary(seleniumWebDriver);
        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldBeAbleToAddBooksWithSameAuthor() {
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        Item seleniumWebDriverBasics = new Book("Mark Collin", "Basics of Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Mastering Selenium WebDriver;Mark Collin;1;1\r\n"
            + "Basics of Selenium WebDriver;Mark Collin;1;1\r\n");

        library.addItemToLibrary(seleniumWebDriver, seleniumWebDriverBasics);
        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldBeAbleToAddBooksWithSameTitle() {
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        Item seleniumWebDriverBasics = new Book("Tom Hanks", "Mastering Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Mastering Selenium WebDriver;Mark Collin;1;1\r\n"
                + "Mastering Selenium WebDriver;Tom Hanks;1;1\r\n");

        library.addItemToLibrary(seleniumWebDriver, seleniumWebDriverBasics);
        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void numberOfBooksShouldIncreaseWhileAddingBooks() {
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Mastering Selenium WebDriver;Mark Collin;3;3\r\n");

        library.addItemToLibrary(seleniumWebDriver);

        library.addItemToLibrary(seleniumWebDriver);

        library.addItemToLibrary(seleniumWebDriver);
        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void numberOfBooksShouldIncreaseWhileAddingBooksWhenRented() {
        User natalia = new Student("Natalia", "Kukulska");
        library.addUserToLibrary(natalia);
        Item seleniumWebDriver = new Book("Mark Collin", "Mastering Selenium WebDriver");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Mastering Selenium WebDriver;Mark Collin;3;2\r\n");

        library.addItemToLibrary(seleniumWebDriver);
        library.rentItemToUser(seleniumWebDriver, natalia);

        library.addItemToLibrary(seleniumWebDriver);
        library.addItemToLibrary(seleniumWebDriver);

        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldBeAbleToAddMagazines() {
        Item snationalGeographic = new Magazine("02/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;02/2011;1;1\r\n");

        library.addItemToLibrary(snationalGeographic);
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldBeAbleToAddMagazinesWithDifferentNumbers() {
        Item nationalGeographic02 = new Magazine("02/2011", "National Geographic");
        Item nationalGeographic05 = new Magazine("05/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;02/2011;1;1\r\n"
            + "National Geographic;05/2011;1;1\r\n");

        library.addItemToLibrary(nationalGeographic02, nationalGeographic05);
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldBeAbleToAddMagazinesWithDifferentTitles() {
        Item nationalGeographic02 = new Magazine("03/2011", "National Geographic");
        Item nationalGeographic05 = new Magazine("03/2011", "Naj");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;03/2011;1;1\r\n"
                + "Naj;03/2011;1;1\r\n");

        library.addItemToLibrary(nationalGeographic02, nationalGeographic05);
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void numberOfBooksShouldIncreaseWhileAddingMagazines() {
        Item nationalGeographic = new Magazine("02/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;02/2011;3;3\r\n");

        library.addItemToLibrary(nationalGeographic);
        library.addItemToLibrary(nationalGeographic);
        library.addItemToLibrary(nationalGeographic);

        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void numberOfMagazinesShouldIncreaseWhileAddingMagazinesWhenRented() {
        User natalia = new Student("Natalia", "Kukulska");
        library.addUserToLibrary(natalia);
        Item nationalGeographic = new Magazine("02/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;02/2011;3;2\r\n");

        library.addItemToLibrary(nationalGeographic);
        library.rentItemToUser(nationalGeographic, natalia);

        library.addItemToLibrary(nationalGeographic);
        library.addItemToLibrary(nationalGeographic);

        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }
}
