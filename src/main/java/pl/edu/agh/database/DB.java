package pl.edu.agh.database;

import pl.edu.agh.base.*;

import java.io.File;
import java.sql.*;
import java.sql.DriverManager;
import java.util.ArrayList;

public class DB {
    private Connection connection;

    private Connection getConnection() {
        return connection;
    }

    private void setConnection(Connection connection) {
        this.connection = connection;
    }

    private Connection connect() {
        try {
            String url = "jdbc:sqlite:library.db";
            connection = DriverManager.getConnection(url);
            setConnection(connection);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public DB() {
        File file = new File("library.db");
        if (!file.exists()) {
//            System.out.println("Database does not exist - creating tables");
            connection = connect();
            checkIfCreated(connection);
        } else {
            Connection connection = connect();
        }

    }

    public void checkIfCreated(Connection connection) {
        PreparedStatement ps = null;
        try {
            connection = connect();

            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS users(" +
                    "id INTEGER NOT NULL PRIMARY KEY, " +
                    "firstname varchar(300) NOT NULL, " +
                    "lastname varchar(300) NOT NULL, " +
                    "cardnumber varchar(10) NOT NULL" +
                    " );");
            ps.executeUpdate();
            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS items(" +
                    "id INTEGER NOT NULL PRIMARY KEY," +
                    "title VARCHAR(300) NOT NULL," +
                    "number VARCHAR(100) ," +
                    "author VARCHAR(100)  ," +
                    "items VARCHAR(10) ," +
                    "type VARCHAR(1) NOT NULL," +
                    "itemsleft VARCHAR(10)" +
                    ");");
            ps.executeUpdate();

            ps = connection.prepareStatement("CREATE TABLE IF NOT EXISTS booking(" +
                    "id INTEGER NOT NULL PRIMARY KEY," +
                    "bookid INTEGER NOT NULL," +
                    "userid INTEGER NOT NULL," +
                    "FOREIGN KEY(bookid) REFERENCES book(id)," +
                    "FOREIGN KEY(userid) REFERENCES users(id)" +
                    ");");
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    public Boolean insertUserToDB(User user) {
        PreparedStatement ps = null;
        String sql = "insert into users(firstname,lastname,cardnumber) values('" + user.getFirstName() + "','" + user.getLastName() + "','" + user.getCardNumber() + "');";
        try {
            connection = connect();
            ps = connection.prepareStatement(sql);
            int numrows = ps.executeUpdate();
            if (numrows == 0) {
                new IllegalStateException();
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
                ps = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;
    }


    public ArrayList<User> getUserDetails() {
        ArrayList lista = new ArrayList();
        Statement stmt = null;
        try {
            connection = connect();
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = stmt.executeQuery("SELECT cardnumber,firstname,lastname FROM users;");
            while (rs.next()) {
                if (rs.getString("cardnumber").substring(0, 2).equals("ST")) {
                    Student st = new Student(rs.getString("firstname"), rs.getString("lastname"));
                    st.setCardNumber(rs.getString("cardnumber"));
                    lista.add(st);
                } else if (rs.getString("cardnumber").substring(0, 2).equals("LE")) {
                    Lecturer le = new Lecturer(rs.getString("firstname"), rs.getString("lastname"));
                    le.setCardNumber(rs.getString("cardnumber"));
                    lista.add(le);
                } else {
                    new IllegalArgumentException("Not compatible user type");
                }

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }


    public ArrayList<String> getBookingDetails() {
        ArrayList lista = new ArrayList();
        PreparedStatement stmt = null;

        try {
            connection = connect();
            String sql = "select userid,bookid from booking";
            stmt = connection.prepareStatement(sql);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("userid") + ";" + rs.getString("bookid"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return lista;
    }


    public ArrayList<Book> getBookDetails() {
        ArrayList lista = new ArrayList();

        Statement stmt = null;
        try {
            connection = connect();
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = stmt.executeQuery("SELECT id,title,number,author,type,items,itemsleft from items;");
            while (rs.next()) {
                if (rs.getString("type").equals("B")) {
                    Book book = new Book(rs.getString("author"), rs.getString("title"));
                    book.setItems(rs.getInt("items"));
                    book.setItemsleft(rs.getInt("itemsleft"));
//                    st.setCardNumber(rs.getString("cardnumber"));
                    lista.add(book);

                } else {
                    new IllegalArgumentException("Not compatible user type");
                }

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public ArrayList<Magazine> getMagazineDetails() {
        ArrayList lista = new ArrayList();
        Statement stmt = null;
        try {
            connection = connect();
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = stmt.executeQuery("SELECT id,title,number,author,type,items,itemsleft from items;");
            while (rs.next()) {
                if (rs.getString("type").equals("M")) {
                    Magazine magazine = new Magazine(rs.getString("number"), rs.getString("title"));
                    magazine.setItems(rs.getInt("items"));
                    magazine.setItemsleft(rs.getInt("itemsleft"));
                    lista.add(magazine);

                } else {
                    new IllegalArgumentException("Not compatible user type");
                }

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public Boolean checkIfItemExists(Item item) {

        ArrayList lista = new ArrayList();
        PreparedStatement stmt = null;
        try {
            connection = connect();

            String sql = "";
            if (item.name().equals("Book")) {
                sql = "SELECT title FROM items where author=? and title=?";
                stmt = connection.prepareStatement(sql);
            } else if (item.name().equals("Magazine")) {
                sql = "SELECT title FROM items where number=? and title=?";
                stmt = connection.prepareStatement(sql);
            }
            stmt.setString(1, item.getAdditionalProperty());
            stmt.setString(2, item.getTitle());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                if (rs.getString("title").length() > 0) {
                    return true;
                }

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return false;
    }


    public int getItemsleft(Item item) {
        try {
            connection = connect();
            PreparedStatement stmt = null;
            String sql = "";
            if (item.name().equals("Book")) {
                sql = "SELECT itemsleft from items where author=? and title=?";
            }
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, item.getAdditionalProperty());
            stmt.setString(2, item.getTitle());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("itemsleft");
            }
            stmt.clearParameters();
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    public String getColumnName(Item item) {
        if (item.name().equals("Book")) {
            return "author";
        } else if (item.name().equals("Magazine")) {
            return "number";
        }

        return "";
    }


    public void insertBooking(Item item, User user) {
        try {
            PreparedStatement stmt = null;
            connection = null;
            connection = connect();
            String sql = "";
            sql = "insert into booking(bookid,userid) select id, (select id from users where " +
                    "firstname=? and " +
                    "lastname=? and " +
                    "cardnumber=?) " +
                    "from items where " +
                    "" + getColumnName(item) + "=? " +
                    "and title=?;";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.setString(3, getCardIdFromUser(user));
            stmt.setString(4, item.getAdditionalProperty());
            stmt.setString(5, item.getTitle());
            stmt.executeUpdate();
            connection = null;
            connection = connect();
            sql = "UPDATE items set itemsleft=itemsleft -1 where " + getColumnName(item) + "=? and title=?;";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, item.getAdditionalProperty());
            stmt.setString(2, item.getTitle());
            stmt.executeUpdate();
            stmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public ArrayList<String> getUserCardNumbers() {
        String sql = "SELECT cardnumber from users;";
        PreparedStatement stmt = null;
        ArrayList<String> lista = new ArrayList();
        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                lista.add(rs.getString("cardnumber"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public String getCardIdFromUser(User user) {
        String cardnumber = "";
        String sql = "SELECT cardnumber from users where firstname=? and lastname=? "; //LIMIT 1;";
        try {
            connection = connect();
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            ResultSet rs = stmt.executeQuery();
            cardnumber = rs.getString("cardnumber");
            rs.close();
            stmt.close();
            return cardnumber;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return "";
    }

    public int getItems(Book book) {

        String sql = "SELECT items from items where type = ? and title =? and author=?;";//'" + book.getTitle() + "' and author='" + book.getAuthor() + "';";
        PreparedStatement stmt = null;

        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, "B");
            stmt.setString(2, book.getTitle());
            stmt.setString(3, book.getAuthor());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("items");

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return 0;

    }
//
//    public int getItems(Book book) {
//
//        String sql = "SELECT items from items where type = ? and title =? and author=?;";//'" + book.getTitle() + "' and author='" + book.getAuthor() + "';";
//        PreparedStatement stmt = null;
//
//        try {
//            connection = connect();
//            stmt = connection.prepareStatement(sql);
//            stmt.setString(1, "B");
//            stmt.setString(2, book.getTitle());
//            stmt.setString(3, book.getAuthor());
//            ResultSet rs = stmt.executeQuery();
//            while (rs.next()) {
//                return rs.getInt("items");
//
//            }
//            rs.close();
//            stmt.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                connection.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//
//        return 0;
//    }

    public int getItems(Magazine magazine) {

        String sql = "SELECT items from items where type=? and title =? and number=?;";
        PreparedStatement stmt = null;

        try {
            connection = connect();
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, "M");
            stmt.setString(2, magazine.getTitle());
            stmt.setString(3, magazine.getNumber());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                return rs.getInt("items");

            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    public ArrayList<String> getCardID() {
        ArrayList lista = new ArrayList();
        Statement stmt = null;
        try {
            connection = connect();
            stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT cardnumber FROM users;");
            while (rs.next()) {
                lista.add(rs.getString("cardnumber"));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public Boolean insertItemToDB(Item i) {
        PreparedStatement ps = null;


        try {
            connection = connect();
            String sql = "";
            if (i.name().equals("Book")) {
                sql = "insert into items(author,title,type,items,itemsleft) values(?,?,?,?,?);";
                ps = connection.prepareStatement(sql);
                ps.setString(1, i.getAdditionalProperty());
                ps.setString(2, i.getTitle());
                ps.setString(3, "B");
                ps.setInt(4, i.getItems());
                ps.setInt(5, i.getItems());
            } else if (i.name().equals("Magazine")) {
                sql = "insert into items(number,title,type,items,itemsleft) values(?,?,?,?,?);";
                ps = connection.prepareStatement(sql);
                ps.setString(1, i.getAdditionalProperty());
                ps.setString(2, i.getTitle());
                ps.setString(3, "M");
                ps.setInt(4, i.getItems());
                ps.setInt(5, i.getItems());
            }
            int numrows = ps.executeUpdate();
            if (numrows == 0) {
                new IllegalStateException();
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;
    }


    public Boolean updateItem(Item item) {
        PreparedStatement ps = null;
        String sql = null;
        try {
            connection = connect();
            sql = "UPDATE items set items=?,itemsleft=itemsleft+?-items where " + getColumnName(item) + "=? and title=?";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, item.getItems());
            ps.setInt(2, item.getItems());
            ps.setString(3, item.getAdditionalProperty());
            ps.setString(4, item.getTitle());
            int numrows = ps.executeUpdate();

            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return true;


    }

    public int getItemsBooked(User user) {
        String sql = "select count(*) as count from booking where userid in(select id from users where cardnumber=?)";
        int result = 0;
        try {
            connection = connect();
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, getCardIdFromUser(user));

            ResultSet rs = stmt.executeQuery();
            result = rs.getInt("count");
//            while (rs.next()) {
//                return rs.getInt("count");
//            }
            rs.close();
            stmt.close();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return 0;
    }


    public ArrayList<Item> getUserBooking(String cardnumber) {
        ArrayList<Item> lista = new ArrayList();
        String sql = "select number,title,author,items,itemsleft,type " +
                "from items i " +
                "join booking b on b.bookid=i.id " +
                "join users u on u.id=b.userid " +
                "where cardnumber=?;";
        try {
            connection = connect();
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, cardnumber);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                if (rs.getString("type").equals("B")) {
                    Book b = new Book(rs.getString("author"), rs.getString("title"));
                    lista.add(b);
                } else if (rs.getString("type").equals("M")) {
                    Magazine m = new Magazine(rs.getString("number"), rs.getString("title"));
                    lista.add(m);
                }
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public Boolean checkIfUserExists(User user) {
        String sql = "select count(*) as count from users where cardnumber=?;";
        int result = 0;
        try {
            connection = connect();
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.getCardNumber());

            ResultSet rs = stmt.executeQuery();
            result = rs.getInt("count");
            rs.close();
            stmt.close();
            return result != 0;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return false;
    }
}
