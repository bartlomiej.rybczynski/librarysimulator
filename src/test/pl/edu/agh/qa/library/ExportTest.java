package pl.edu.agh.qa.library;


import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import pl.edu.agh.base.*;
import pl.edu.agh.qa.library.utils.DisplayedPerson;
import pl.edu.agh.qa.library.utils.ExportedDataParser;
import pl.edu.agh.qa.library.utils.ExportedItem;
import pl.edu.agh.qa.library.utils.PrintHandler;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ExportTest {
    private static final String EXPORT_FILE_NAME_1 = "exportedResults1.txt";
    private static final String EXPORT_FILE_NAME_2 = "exportedResults2.txt";
    private static final String EXPORT_FILE_NAME_3 = "exportedResults3.txt";
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }

    @Test
    public void shouldExportAllItemsOfStudent() throws IOException {
        final String expectedFilePath = "/expectedExport1.txt";

        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertTrue(library.rentItemToUser(nationalGeographic, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(nationalGeographic, student));
        assertFalse(library.rentItemToUser(ogniemIMieczem, student));
        library.exportUsersWithItemsToFile(EXPORT_FILE_NAME_1);

        List<ExportedItem> exportedItemList = ExportedDataParser.getExportedItems(EXPORT_FILE_NAME_1);
        List<ExportedItem> expectedItemList = ExportedDataParser.getExpectedItems(expectedFilePath);

        TestCase.assertTrue(ExportedDataParser.itemsListHaveSameItems(exportedItemList, expectedItemList));
    }

    @Test
    public void shouldExportAllItemsOfLecturer() throws IOException {
        final String expectedFilePath = "/expectedExport2.txt";
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User lecturer = new Lecturer("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(lecturer);

        assertTrue(library.rentItemToUser(nationalGeographic, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(graOTron, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(nationalGeographic, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        library.exportUsersWithItemsToFile(EXPORT_FILE_NAME_2);

        List<ExportedItem> exportedItemList = ExportedDataParser.getExportedItems(EXPORT_FILE_NAME_2);
        List<ExportedItem> expectedItemList = ExportedDataParser.getExpectedItems(expectedFilePath);

        TestCase.assertTrue(ExportedDataParser.itemsListHaveSameItems(exportedItemList, expectedItemList));
    }

    @Test
    public void shouldExportDataForAFewUsers() throws IOException {
        final String expectedFilePath = "/expectedExport3.txt";
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User lecturer = new Lecturer("Adam", "Kowalski");
        User student1 = new Student("Tomasz", "Wazny");
        User student2 = new Student("Marek", "Kujawski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(lecturer, student1, student2);

        assertTrue(library.rentItemToUser(nationalGeographic, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(graOTron, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(nationalGeographic, student1));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student1));
        assertTrue(library.rentItemToUser(nationalGeographic, student2));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student2));
        library.exportUsersWithItemsToFile(EXPORT_FILE_NAME_3);

        List<ExportedItem> exportedItemList = ExportedDataParser.getExportedItems(EXPORT_FILE_NAME_3);
        List<ExportedItem> expectedItemList = ExportedDataParser.getExpectedItems(expectedFilePath);

        TestCase.assertTrue(ExportedDataParser.itemsListHaveSameItems(exportedItemList, expectedItemList));

        library.printListOfUsers();
        List<String> expectedCardList = getCardNumberListFromDisplay(PrintHandler.getDisplayedPersonsList(systemOutRule.getLog()));
        List<String> actualCardList = getCardNumberListFromFile(exportedItemList);

        assertTrue(expectedCardList.size() == actualCardList.size());
    }

    private List<String> getCardNumberListFromFile(List<ExportedItem> itemsList) {
        return itemsList.stream().map(ExportedItem::getCardNumber).collect(Collectors.toList());

    }

    private List<String> getCardNumberListFromDisplay(List<DisplayedPerson> itemsList) {
        return itemsList.stream().map(DisplayedPerson::getCardNr).collect(Collectors.toList());

    }

    private boolean sameStringsAreInLists(List<String> expectedList, List<String> actualList) {
        if (expectedList.size()!=actualList.size()) {
            return false;
        }
        for (String item : actualList) {
            if (!expectedList.contains(item)) {
                return false;
            }
        }
        return true;
    }
}
