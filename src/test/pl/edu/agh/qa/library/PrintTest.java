package pl.edu.agh.qa.library;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import pl.edu.agh.base.*;
import pl.edu.agh.qa.library.utils.DisplayedItem;
import pl.edu.agh.qa.library.utils.DisplayedPerson;
import pl.edu.agh.qa.library.utils.PrintHandler;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class PrintTest {
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }

    @Test
    public void usersShouldBePrintedWithProperFormat() {
        //given
        User jan = new Student("Jan", "Kowalski");
        User grzegorz = new Lecturer("Grzegorz", "Szczutkowski");
        User natalia = new Student("Natalia", "Kukulska");
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Grzegorz;Szczutkowski;ID2;L\r\n"
                + "Jan;Kowalski;ID1;S\r\n"
                + "Natalia;Kukulska;ID2;S\r\n");

        //when
        library.addUserToLibrary(jan, grzegorz, natalia);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void booksShouldBePrintedInProperFormat() {
        library.importItemsFromFile("books.csv");
        User jan = new Student("Jan", "Kowalski");
        library.addUserToLibrary(jan);
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;2;0\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;20\r\n");

        library.addItemToLibrary(graOTron);
        library.rentItemToUser(graOTron, jan);
        library.rentItemToUser(graOTron, jan);
        library.rentItemToUser(nationalGeographic, jan);
        library.printListOfBooks();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void magazinesShouldBePrintedInProperFormat() {
        library.importItemsFromFile("books.csv");
        User jan = new Student("Jan", "Kowalski");
        library.addUserToLibrary(jan);
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item nationalGeographic01 = new Magazine("01/2011", "National Geographic");
        Item nationalGeographic04 = new Magazine("04/2011", "National Geographic");
        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("National Geographic;04/2011;1;1\r\n" +
                "National Geographic;01/2011;6;5\r\n");

        library.addItemToLibrary(graOTron, nationalGeographic01, nationalGeographic04);
        library.rentItemToUser(graOTron, jan);
        library.rentItemToUser(graOTron, jan);
        library.rentItemToUser(nationalGeographic01, jan);
        library.printListOfMagazines();

        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }
}
