package pl.edu.agh.qa.library.utils;


import pl.edu.agh.qa.library.ExportTest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ExportedDataParser {
    private static final String ITEMS_SEPARATOR = ";";
    private static final String TITLE_AUTHOR_SEPARATOR = "-";

    public static List<ExportedItem> getExportedItems(String exportedFilePath) {
        List<ExportedItem> exportedItems = new ArrayList<>();
        File fileWithBooks = new File(exportedFilePath);
        try (FileReader fileReader = new FileReader(fileWithBooks);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String pattern = "(.+)\\[(.+)\\]";
                Pattern r = Pattern.compile(pattern);
                Matcher m = r.matcher(line);
                if (m.find( )) {
                    String cardNumber = m.group(1).trim();
                    List<String> items = Stream.of(m.group(2).split(ITEMS_SEPARATOR))
                            .map(String::trim)
                            .collect(Collectors.toList());
                    ExportedItem item = new ExportedItem(cardNumber, items);
                    exportedItems.add(item);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exportedItems;
    }

    public static List<ExportedItem> getExpectedItems(String expectedFilePathInResources) {
        URL resource = ExportTest.class.getResource(expectedFilePathInResources);
        try {
            return getExportedItems(Paths.get(resource.toURI()).toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean itemsListHaveSameItems(List<ExportedItem> expectedList, List<ExportedItem> actualList) {
        if (expectedList.size()!=actualList.size()) {
            return false;
        }
        for (ExportedItem item : actualList) {
            if (!expectedList.contains(item)) {
                return false;
            }
        }
        return true;
    }


}
