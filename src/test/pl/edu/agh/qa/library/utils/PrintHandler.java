package pl.edu.agh.qa.library.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class PrintHandler {
    private static final String ITEM_PERSON_LINE_REGEXP = ".+?;.+?;.+?;.+?\r{0,1}\n{0,1}\r{0,1}";

    public static List<DisplayedItem> getDisplayedItemsList(String displayedItems) {
        String[] lines = displayedItems.split("\n");
        List<DisplayedItem> itemsList = new ArrayList<>();

        for (String line : lines) {
            if (line.matches(ITEM_PERSON_LINE_REGEXP)) {
                itemsList.add(new DisplayedItem(line));
            }
        }
        return itemsList;
    }

    public static List<DisplayedPerson> getDisplayedPersonsList(String displayedPersons) {
        String[] lines = displayedPersons.split("\r{0,1}\n");
        List<DisplayedPerson> personsList = new ArrayList<>();

        for (String line : lines) {
            if (line.matches(ITEM_PERSON_LINE_REGEXP)) {
                personsList.add(new DisplayedPerson(line));
            }
        }
        return personsList;
    }

    public static Set<String> getCardNumbersSet(List<DisplayedPerson> displayedPersonList) {
        Set<String> cardNumbersSet = new HashSet<>();
        for (DisplayedPerson person : displayedPersonList) {
            cardNumbersSet.add(person.getCardNr());
        }
        return cardNumbersSet;
    }

    public static boolean itemsListHaveSameItems(List<DisplayedItem> expectedList, List<DisplayedItem> actualList) {
        if (expectedList.size()!=actualList.size()) {
            return false;
        }
        for (DisplayedItem item : actualList) {
            if (!expectedList.contains(item)) {
                return false;
            }
        }
        return true;
    }

    public static boolean userListHaveSameItems(List<DisplayedPerson> expectedList, List<DisplayedPerson> actualList) {
        if (expectedList.size()!=actualList.size()) {
            return false;
        }
        for (DisplayedPerson user : actualList) {
            if (!expectedList.contains(user)) {
                return false;
            }
        }
        return true;
    }
}
