package pl.edu.agh.qa.library;


import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import pl.edu.agh.base.Lecturer;
import pl.edu.agh.base.Library;
import pl.edu.agh.base.Student;
import pl.edu.agh.base.User;
import pl.edu.agh.qa.library.utils.DisplayedPerson;
import pl.edu.agh.qa.library.utils.PrintHandler;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class AddUserTest {
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }

    @Test
    public void shouldBeAbleToAddStudent() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Jan;Kowalski;ID1;S\r\n");

        User jan = new Student("Jan", "Kowalski");
        library.addUserToLibrary(jan);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void shouldBeAbleToAddLecturer() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Grzegorz;Szczutkowski;ID2;L\r\n");

        User grzegorz = new Lecturer("Grzegorz", "Szczutkowski");
        library.addUserToLibrary(grzegorz);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void shouldBeAbleToAddStudentAndLecturer() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Grzegorz;Szczutkowski;ID2;L\r\n"
                + "Jan;Kowalski;ID1;S\r\n"
                + "Natalia;Kukulska;ID2;S\r\n");

        User jan = new Student("Jan", "Kowalski");
        User grzegorz = new Lecturer("Grzegorz", "Szczutkowski");
        User natalia = new Student("Natalia", "Kukulska");
        library.addUserToLibrary(jan, grzegorz, natalia);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void shouldBeAbleToAddStudentsWithSameNames() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Jan;Kowalski;ID1;S\r\n"
                + "Jan;Kowalski;ID2;S\r\n");

        User jan1 = new Student("Jan", "Kowalski");
        User jan2 = new Student("Jan", "Kowalski");
        library.addUserToLibrary(jan1, jan2);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void shouldBeAbleToAddLecturersWithSameNames() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Jan;Kowalski;ID1;L\r\n"
                + "Jan;Kowalski;ID2;L\r\n");

        User jan1 = new Lecturer("Jan", "Kowalski");
        User jan2 = new Lecturer("Jan", "Kowalski");
        library.addUserToLibrary(jan1, jan2);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

    @Test
    public void shouldBeAbleToAddUsersWithSameNames() {
        List<DisplayedPerson> expectedList = PrintHandler.getDisplayedPersonsList("Jan;Kowalski;ID1;L\r\n"
                + "Jan;Kowalski;ID2;S\r\n");

        User jan1 = new Lecturer("Jan", "Kowalski");
        User jan2 = new Student("Jan", "Kowalski");
        library.addUserToLibrary(jan1, jan2);
        library.printListOfUsers();

        //then
        List<DisplayedPerson> actualList = PrintHandler.getDisplayedPersonsList(systemOutRule.getLog());

        assertTrue(PrintHandler.userListHaveSameItems(expectedList, actualList));
        TestCase.assertEquals(PrintHandler.getCardNumbersSet(actualList).size(), expectedList.size());
    }

}
