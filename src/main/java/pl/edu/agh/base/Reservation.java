package pl.edu.agh.base;

public class Reservation {
    private Item i;
    private String u;

    public Reservation(User userId, Item item) {
        this.u = userId.getCardNumber();
        this.i = item;
    }

    public Item getItem(){
        return i;
    }

    public String getUserID(){
        return u;
    }

}
