package pl.edu.agh.base;

public class Magazine extends Item{
    String number;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String name(){

        return "Magazine";
    }

    @Override
    public String getAdditionalProperty() {
        return getNumber();
    }

    @Override
    public void setAdditionalProperty(String number) {
        setNumber(number);
    }

    public Magazine(String number, String title){
        super(title);
        setNumber(number);

    }
}
