package pl.edu.agh.base;

import pl.edu.agh.database.DB;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public abstract class User {
    private String FirstName;

    private String LastName;
    private int howManyItemsCanBeRented;
    private String cardNumber;

    public User(String FirstName, String LastName){
        setFirstName(FirstName);
        setLastName(LastName);
        String cardNumber = GenerateCardNumber();
        while(checkIfCardExists(cardNumber)){
            System.out.println("Karta istnieje! Losuje nastepna");
            cardNumber = GenerateCardNumber();
        }
        setCardNumber(cardNumber);
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getHowManyItemsCanBeRented() {
        return howManyItemsCanBeRented;
    }

    public void setHowManyItemsCanBeRented(int howManyItemsCanBeRented) {
        this.howManyItemsCanBeRented = howManyItemsCanBeRented;
    }

    public String getCardNumber() {
        return cardNumber;
    }
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }


    private Boolean checkIfCardExists(String cardnumber) {
        DB db = new DB();
        ArrayList<String> lista = new ArrayList();
        lista = db.getCardID();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(cardnumber)) {
                return true;
            }
        }
        return false;
    }

    private String GenerateCardNumber(){
        String  CardNumber = "0" ;
        LocalDateTime date = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSS");
        CardNumber = getName().substring(0,2).toUpperCase() + date.format(formatter);
        return   CardNumber;
    }
    public abstract int NumberOfPositionsToRent();
    public abstract String getName();

}
