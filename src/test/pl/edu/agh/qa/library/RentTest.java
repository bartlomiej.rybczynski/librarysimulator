package pl.edu.agh.qa.library;

import pl.edu.agh.base.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import pl.edu.agh.qa.library.utils.DisplayedItem;
import pl.edu.agh.qa.library.utils.PrintHandler;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class RentTest {
    private Library library;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void setUp() {
        systemOutRule.getLogWithNormalizedLineSeparator();
        library = new Library();
    }

    public static void main(String[] args) throws IOException {
        Library library = new Library();
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);
        boolean rented = library.rentItemToUser(ogniemIMieczem, student);
    }

    @Test
    public void shouldNotRentMoreThen4BooksToStudent() {
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertFalse(library.rentItemToUser(ogniemIMieczem, student));
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;1\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;16\r\n" +
                "National Geographic;01/2011;5;5\r\n");
        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        Assert.assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldNotRentMoreThen4ItemsToStudent() {
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertTrue(library.rentItemToUser(nationalGeographic, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(ogniemIMieczem, student));
        assertTrue(library.rentItemToUser(nationalGeographic, student));
        assertFalse(library.rentItemToUser(ogniemIMieczem, student));
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;1\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;18\r\n" +
                "National Geographic;01/2011;5;3\r\n");
        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        Assert.assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldNotRentMoreThen10ItemsToLecturer() {
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User lecturer = new Lecturer("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(lecturer);

        assertTrue(library.rentItemToUser(nationalGeographic, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(graOTron, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(nationalGeographic, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertFalse(library.rentItemToUser(ogniemIMieczem, lecturer));
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;0\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;13\r\n" +
                "National Geographic;01/2011;5;3\r\n");
        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        Assert.assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldNotRentMoreThen10BooksToLecturer() {
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item ogniemIMieczem = new Book("H. Sienkiewicz", "Ogniem i mieczem");
        User lecturer = new Lecturer("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(lecturer);

        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(graOTron, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertTrue(library.rentItemToUser(ogniemIMieczem, lecturer));
        assertFalse(library.rentItemToUser(ogniemIMieczem, lecturer));
        library.printListOfBooks();
        library.printListOfMagazines();

        List<DisplayedItem> expectedList = PrintHandler.getDisplayedItemsList("Tożsamość Bourne’a;R. Ludlum;10;10\r\n" +
                "Gra o tron;George’a R.R. Martin;1;0\r\n" +
                "Ogniem i mieczem;H. Sienkiewicz;20;11\r\n" +
                "National Geographic;01/2011;5;5\r\n");
        List<DisplayedItem> actualList = PrintHandler.getDisplayedItemsList(systemOutRule.getLog());

        Assert.assertTrue(PrintHandler.itemsListHaveSameItems(actualList, expectedList));
    }

    @Test
    public void shouldNotRentNotAvailableBook() {
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertTrue(library.rentItemToUser(nationalGeographic, student));
        assertTrue(library.rentItemToUser(graOTron, student));
        assertFalse(library.rentItemToUser(graOTron, student));
    }

    @Test
    public void shouldNotRentNotExistingBook() {
        Item notExistingBook = new Book("George’a R.R. Martin", "Not existing book");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertFalse(library.rentItemToUser(notExistingBook, student));
    }

    @Test
    public void shouldNotRentBookFromEmptyLibrary() {
        Item notExistingBook = new Book("George’a R.R. Martin", "Not existing book");
        User student = new Student("Adam", "Kowalski");
        library.addUserToLibrary(student);

        assertFalse(library.rentItemToUser(notExistingBook, student));
    }

    @Test
    public void shouldNotRentBookToNotAddedUser() {
        Item graOTron = new Book("George’a R.R. Martin", "Gra o tron");
        User student = new Student("Adam", "Kowalski");
        library.addItemToLibrary(graOTron);

        assertFalse(library.rentItemToUser(graOTron, student));
    }

    @Test
    public void shouldNotRentNotAvailableMagazine() {
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User adam = new Student("Adam", "Kowalski");
        User natalia = new Student("Natalia", "Kukulska");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(adam, natalia);

        assertTrue(library.rentItemToUser(nationalGeographic, natalia));
        assertTrue(library.rentItemToUser(nationalGeographic, natalia));
        assertTrue(library.rentItemToUser(nationalGeographic, natalia));
        assertTrue(library.rentItemToUser(nationalGeographic, natalia));
        library.printListOfMagazines();
        assertTrue(library.rentItemToUser(nationalGeographic, adam));
        assertFalse(library.rentItemToUser(nationalGeographic, adam));
    }

    @Test
    public void shouldNotRentNotExistingMagazine() {
        Item nationalGeographic = new Magazine("01/2011", "Not existing magazine");
        User student = new Student("Adam", "Kowalski");
        library.importItemsFromFile("books.csv");
        library.addUserToLibrary(student);

        assertFalse(library.rentItemToUser(nationalGeographic, student));
    }

    @Test
    public void shouldNotRentMagazineFromEmptyLibrary() {
        Item notExistingBook = new Magazine("01/2011", "National Geographic");
        User student = new Student("Adam", "Kowalski");
        library.addUserToLibrary(student);

        assertFalse(library.rentItemToUser(notExistingBook, student));
    }

    @Test
    public void shouldNotRentMagazineToNotAddedUser() {
        Item nationalGeographic = new Magazine("01/2011", "National Geographic");
        User student = new Student("Adam", "Kowalski");
        library.addItemToLibrary(nationalGeographic);

        assertFalse(library.rentItemToUser(nationalGeographic, student));
    }
}
