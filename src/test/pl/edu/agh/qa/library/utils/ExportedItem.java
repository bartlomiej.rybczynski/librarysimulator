package pl.edu.agh.qa.library.utils;

import java.util.List;
import java.util.Objects;

public class ExportedItem {
    private String cardNumber;
    private List<String> rentedItems;

    public ExportedItem(String cardNumber, List<String> rentedItems) {
        this.cardNumber = cardNumber;
        this.rentedItems = rentedItems;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public List<String> getRentedItems() {
        return rentedItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ExportedItem that = (ExportedItem) o;
        return itemsListHaveSameItems(rentedItems, that.rentedItems);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cardNumber);
    }

    private boolean itemsListHaveSameItems(List<String> expectedList, List<String> actualList) {
        if (expectedList.size()!=actualList.size()) {
            return false;
        }
        for (String item : actualList) {
            if (!expectedList.contains(item)) {
                return false;
            }
        }
        return true;
    }
}
