Stwórz w języku programowania java( projekt LibrarySimulator, który będzie służył do obsługi
biblioteki. Wymagania projektu są następujące:
1. Możliwość dodawania nowych użytkowników biblioteki. Użytkownicy dzielą się na dwa typy:
• pl.edu.agh.base.Student - może wypożyczyć maksymalnie 4 książki w jednym momencie
• Wykładowa - może wypożyczyć do 10 książek jednocześnie
Każdy użytkownik musi posiadać imię i nazwisko i numer karty w bibliotece. Może być dwóch
użytkowników z tym samym imieniem i nazwiskiem ale numer karty w bibliotece musi być unikalny.
Numer karty jest generowany automatycznie w momencie dodawania użytkownika do biblioteki – nie
ma znaczenia jego format.
2. Możliwość dodawania do zbioru w bibliotece dwóch rodzajów elementów:
• Książka – każda musi posiadać tytuł i autora
• Magazyn – każda musi posiadać tytuł i numer magazynu
3. Jeżeli dodawana jest istniejąca książka/magazyn to powinna się zwiększyć ilość wszystkich
dostępnych egzemplarzy tej książki/magazynu.
4. Metoda dodawania Książek i Magazynów powinna mieć możliwość dodania jednego jak i kilku
rodzajów elementów niezależnie od typu(książka, magazyn).
5. Możliwość wypożyczania Książek i Magazynów na rzecz użytkownika, jeśli w bibliotece znajduje się
wystarczająca liczba egzemplarzy do wypożyczenia i użytkownik ma możliwość wypożyczenia
kolejnego egzemplarza.
6. Możliwość wypisania na ekranie wszystkich Magazynów z biblioteki. Każdy wiersz dotyczy jednego
numeru i zawiera w następującej kolejności tytuł, numer, ilość wszystkich egzemplarzy oraz ilość
dostępnych egzemplarzy (oddzielone średnikami)
np.
Traveler;03/2017;30;17
National Geographic;01/2016;15;13
7. Możliwość wypisania na ekranie wszystkich Książek z biblioteki. Każdy wiersz dotyczy jednego tytułu
i zawiera w następującej kolejności tytuł, autora, ilość wszystkich egzemplarzy oraz ilość dostępnych
egzemplarzy (oddzielone średnikami)
np.
Ogniem i mieczem;H. Sienkiewicz;20;7
Tożsamość Bourne’a;R. Ludlum;40;33
8. Możliwość wypisania wszystkich użytkowników biblioteki. Każdy wiersz dotyczy innego użytkownika
i zawiera w następującej kolejności imię, nazwisko, numer karty w bibliotece oraz typ (oddzielone
średnikami). Typ może mieć wartość S jeżeli użytkownikiem jest student lub L gdy użytkownikiem jest
wykładowca
np.
Grzegorz;Szczutkowski;21;L
Piotr;Kowalczyk;13;S
Maria;Nowak;11;S
9. Możliwość dodawania listy książek/magazynów z pliku tekstowego. W każdym wierszu w pliku
powinien być tytuł, autor/numer, ilość egzemplarzy do dodania i typ (B – książka, M – magazyn)
oddzielone średnikiem. Jeśli są już w bibliotece jakieś książki z pliku tekstowego to wtedy zwiększamy
liczbę egzemplarzy dla tych pozycji. Poniżej przykładowa zawartość pliku z książkami:
Ogniem i mieczem;H. Sienkiewicz;20;B
Tożsamość Bourne’a;R. Ludlum;10;B
Gra o tron;George’a R.R. Martin;15;B
National Geographic;01/2016;15;M
10. Możliwość zapisania do nowego pliku numerów kart tych użytkowników biblioteki, którzy mają
pożyczone egzemplarze i dla każdego użytkownika wypisane tytuły i autora książek lub tytuły i
numery magazynu, które posiada. Jeżeli ktoś posiada kilka takich samych egzemplarzy książki lub
magazynu to powinien on być wyświetlony kilka razy
np.
ID5[Ogniem i mieczem-H. Sienkiewicz; Ogniem i mieczem-H. Sienkiewicz; Traveler-03/2017; National
Geographic-01/2016]
ID24[Traveler-03/2017; National Geographic-01/2016]
Wymagania co do nazewnictwa klas i ich metod:
class pl.edu.agh.base.Library
public pl.edu.agh.base.Library() Konstruktor klasy obiektu typu pl.edu.agh.base.Library
public void addUserToLibrary(pl.edu.agh.base.User... users) Metoda do dodawania Studentów oraz Nauczycieli do bazy biblioteki
(punkt 1).
public void addItemToLibrary(pl.edu.agh.base.Item... item) Metoda do dodawania Książek oraz Magazynów do bazy biblioteki
(punkt 2, 3, 4).
public boolean rentItemToUser(pl.edu.agh.base.Item item, pl.edu.agh.base.User
user)
Metoda do wypożyczania Książki lub Magazynu na rzecz Studenta lub
Nauczyciela. Metoda zwraca 'true' jeżeli akcja wykonana zostanie
poprawnie oraz 'false' gdy z jakiegoś powodu nie można wykonać akcji
(punkt 5).
public void printListOfMagazines() Metoda wypisująca na ekranie listę wszystkich magazynów z biblioteki
z ilością wszystkich oraz dostępnych egzemplarzy(punkt 6).
public void printListOfBooks() Metoda wypisująca na ekranie listę wszystkich książek z biblioteki z
ilością wszystkich oraz dostępnych egzemplarzy(punkt 7).
public void importItemsFromFile(String csvFile) Metoda do importowania z pliku książek lub magazynów (punkt 9).
public void exportUsersWithItemsToFile(String
csvFile)
Metoda do zapisywania w pliku użytkowników z wypożyczonymi
egzemplarzami książek i magazynów (punkt 10).
class pl.edu.agh.base.Magazine extends pl.edu.agh.base.Item
public pl.edu.agh.base.Magazine(String number, String title) Konstruktor tworzący obiekt typu pl.edu.agh.base.Magazine (punkt 1).
class pl.edu.agh.base.Book extends pl.edu.agh.base.Item
public pl.edu.agh.base.Book(String author, String title) Konstruktor tworzący obiekt typu pl.edu.agh.base.Book (punkt 1).
class pl.edu.agh.base.Student extends pl.edu.agh.base.User
public pl.edu.agh.base.Student(String firstName, String lastName) Konstruktor tworzący obiekt typu pl.edu.agh.base.Student (punkt 1).
class pl.edu.agh.base.Lecturer extends pl.edu.agh.base.User
public pl.edu.agh.base.Lecturer(String firstName, String lastName) Konstruktor tworzący obiekt typu pl.edu.agh.base.Lecturer (punkt 1).